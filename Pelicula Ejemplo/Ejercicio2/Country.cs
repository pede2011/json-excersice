﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace Ejercicio1
{
    class Country
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Ingrese nombre de ciudad a buscar: ");
            string ciudad = Console.ReadLine();

            string toSearch = "https://maps.googleapis.com/maps/api/geocode/json?address=" + ciudad+"&language=es";//el &language=es es para que los resultados se muestren
                                                                                                                   //en español
            WebRequest req = WebRequest.Create(toSearch.ToLower());//cast as lower case string for API purposes

            try
            {
                WebResponse respuesta = req.GetResponse();

                Stream stream = respuesta.GetResponseStream();
                StreamReader sr = new StreamReader(stream);

                JObject data = JObject.Parse(sr.ReadToEnd());

                string country = (string)data["status"];

                switch (country)
                {
                    case "ZERO_RESULTS":
                        Console.WriteLine("Esta ciudad no se puede asociar a ningun país");
                        break;

                    case "INVALID_REQUEST":
                        Console.WriteLine("Esta ciudad no es encuentra en la base de datos");
                        break;

                    case "OK":
                        int x;
                        bool successfullyParsed = Int32.TryParse(ciudad, out x);
                        if (successfullyParsed)
                        {
                            country = (string)data["results"][0]["address_components"][1]["long_name"];
                            string numOut = "El número " + ciudad + " esta asociado a " + country;
                            Console.WriteLine(numOut);
                            string ciudadOut = "Ciudad: " + country;
                            Console.WriteLine(ciudadOut);
                            country = (string)data["results"][0]["address_components"][3]["long_name"];
                            string paisOut = "País: " + country;
                            Console.WriteLine(paisOut);
                        }
                        else
                        {
                            country = (string)data["results"][0]["address_components"][1]["long_name"];
                            string ciudadOut = "Ciudad: " + country;
                            Console.WriteLine(ciudadOut);
                            country = (string)data["results"][0]["address_components"][3]["long_name"];
                            string paisOut = "País: " + country;
                            Console.WriteLine(paisOut);
                        }
                        break;
                }
            }

            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.Write("No se pudo conectar. Intente despues...");
                }
                else
                {
                    Console.Write("Error");
                }
            }
            Console.ReadKey();
        }
    }       
}
