﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
                   
                Console.WriteLine("Ingrese titulo de pelicula a buscar: ");
                string titulo = Console.ReadLine();

                string toSearch = "http://www.omdbapi.com/?t=" + titulo;
                
                WebRequest req = WebRequest.Create(toSearch.ToLower());//cast as lower case string for API purposes

            try
            {
                WebResponse respuesta = req.GetResponse();

                Stream stream = respuesta.GetResponseStream();
                StreamReader sr = new StreamReader(stream);

                JObject data = JObject.Parse(sr.ReadToEnd());

                string response = (string)data["Response"];

                switch (response)
                {
                    case "False":
                        Console.WriteLine("Esta pelicula no se encuentra en la base de datos");
                        break;                                           

                    case "True":
                        string title = (string)data["Title"];
                        Console.WriteLine("Título: " + title);

                        string año = (string)data["Year"];
                        Console.WriteLine("Año: " + año);

                        string ratings = (string)data["imdbRating"];
                        Console.WriteLine("Puntuacion: " +ratings+ "/10");
                        break;
                }
            }

            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.Write("No se pudo conectar. Intente despues...");
                }
                else
                {
                    Console.Write("Error");
                }
            }

            Console.ReadKey();           
        }
    }
}
